package main

import (
	"cmp"
	"fmt"
	"math/big"
)

const (
	floatS = 11  // short (float16)
	floatF = 24  // single (float32)
	floatD = 53  // double (float64)
	floatL = 113 // long (float128)

	floatE = floatD // default (at least double)
)

// Types ///////////////////////////////////////////////////////////////////////

type Number interface {
	Datum
	isNumber()

	toLowerPrec(Number) Number

	IsComplex() bool
	IsReal() bool
	IsRational() bool
	IsInteger() bool

	Neg() Number
	Inv() Number
	Abs() Number
}

type _Real[T any] interface {
	*T

	Sign() int
	IsInt() bool
	Add(a, b *T) *T // a+b
	Mul(a, b *T) *T // a*b
	Neg(a *T) *T    // -a
	Sub(a, b *T) *T // a-b
	//Inv(a *T) *T) // 1/a
	Quo(a, b *T) *T // a/b
	Abs(a *T) *T    // |a|
}

type _Number[T any, PT _Real[T]] struct {
	RealPart PT
	ImagPart PT
}

type ExactNumber = _Number[big.Rat, *big.Rat]
type InexactNumber = _Number[big.Float, *big.Float]

var (
	_ Number = (*ExactNumber)(nil)
	_ Number = (*InexactNumber)(nil)
)

// Interface implementation ////////////////////////////////////////////////////

// String implements [fmt.Stringer] and thus [Datum] and thus
// [Number].
func (x *_Number[T, PT]) String() string {
	// TODO
	return ""
}

// isDatum implements [Datum] and thus [Number].
func (*_Number[T, PT]) isDatum() {}

// isNumber implements [Number].
func (*_Number[T, PT]) isNumber() {}

func min[T cmp.Ordered](a, b T) T {
	if a < b {
		return a
	}
	return b
}

// toLowerPrec implements [Number].
func (x *_Number[T, PT]) toLowerPrec(_other Number) Number {
	other, isInexact := _other.(*InexactNumber)
	if !isInexact {
		return x
	}
	switch x := any(x).(type) {
	case *ExactNumber:
		return &InexactNumber{
			RealPart: new(big.Float).SetPrec(other.RealPart.Prec()).SetRat(x.RealPart),
			ImagPart: new(big.Float).SetPrec(other.ImagPart.Prec()).SetRat(x.ImagPart),
		}
	case *InexactNumber:
		if x.RealPart.Prec() < other.RealPart.Prec() && x.ImagPart.Prec() < other.ImagPart.Prec() {
			return x
		}
		ret := *x
		if x.RealPart.Prec() > other.RealPart.Prec() {
			ret.RealPart = new(big.Float).SetPrec(other.RealPart.Prec()).Set(x.RealPart)
		}
		if x.ImagPart.Prec() > other.ImagPart.Prec() {
			ret.ImagPart = new(big.Float).SetPrec(other.ImagPart.Prec()).Set(x.ImagPart)
		}
		return &ret
	default:
		panic(fmt.Errorf("should not happen: %T", x))
	}
}

// Procedures //////////////////////////////////////////////////////////////////

// Page 21, column 2

func IsNumber(x Datum) bool { _, ok := x.(Number); return ok }

func (x *_Number[T, PT]) IsComplex() bool  { return true }
func (x *_Number[T, PT]) IsReal() bool     { return x.ImagPart.Sign() == 0 }
func (x *_Number[T, PT]) IsRational() bool { return x.IsReal() }
func (x *_Number[T, PT]) IsInteger() bool  { return x.IsRational() && x.RealPart.IsInt() }

func IsExact(x Datum) bool { _, ok := x.(*ExactNumber); return ok }

// Page 22, column 1

func Add(a, b Number) Number {
	switch a := a.toLowerPrec(b).(type) {
	case *ExactNumber:
		return add(a, b.toLowerPrec(a).(*ExactNumber))
	case *InexactNumber:
		return add(a, b.toLowerPrec(a).(*InexactNumber))
	default:
		panic(fmt.Errorf("should not happen: %T", a))
	}
}

func add[T any, PT _Real[T]](a, b *_Number[T, PT]) *_Number[T, PT] {
	return &_Number[T, PT]{
		RealPart: PT(new(T)).Add(a.RealPart, b.RealPart),
		ImagPart: PT(new(T)).Add(a.ImagPart, b.ImagPart),
	}
}

func Mul(a, b Number) Number {
	switch a := a.toLowerPrec(b).(type) {
	case *ExactNumber:
		return mul(a, b.toLowerPrec(a).(*ExactNumber))
	case *InexactNumber:
		return mul(a, b.toLowerPrec(a).(*InexactNumber))
	default:
		panic(fmt.Errorf("should not happen: %T", a))
	}
}
func mul[T any, PT _Real[T]](a, b *_Number[T, PT]) *_Number[T, PT] {
	return &_Number[T, PT]{
		RealPart: PT(new(T)).Mul(a.RealPart, b.RealPart),
		ImagPart: PT(new(T)).Mul(a.ImagPart, b.ImagPart),
	}
}

func (x *_Number[T, PT]) Neg() Number {
	return &_Number[T, PT]{
		RealPart: PT(new(T)).Neg(x.RealPart),
		ImagPart: PT(new(T)).Neg(x.ImagPart),
	}
}

func Sub(a, b Number) Number {
	switch a := a.toLowerPrec(b).(type) {
	case *ExactNumber:
		return sub(a, b.toLowerPrec(a).(*ExactNumber))
	case *InexactNumber:
		return sub(a, b.toLowerPrec(a).(*InexactNumber))
	default:
		panic(fmt.Errorf("should not happen: %T", a))
	}
}
func sub[T any, PT _Real[T]](a, b *_Number[T, PT]) *_Number[T, PT] {
	return &_Number[T, PT]{
		RealPart: PT(new(T)).Sub(a.RealPart, b.RealPart),
		ImagPart: PT(new(T)).Sub(a.ImagPart, b.ImagPart),
	}
}

func (x *_Number[T, PT]) Inv() Number {
	switch x := any(x).(type) {
	case *ExactNumber:
		return &ExactNumber{
			RealPart: new(big.Rat).Inv(x.RealPart),
			ImagPart: new(big.Rat).Inv(x.ImagPart),
		}
	case *InexactNumber:
		return &InexactNumber{
			RealPart: new(big.Float).Quo(big.NewFloat(1), x.RealPart),
			ImagPart: new(big.Float).Quo(big.NewFloat(1), x.ImagPart),
		}
	default:
		panic(fmt.Errorf("should not happen: %T", x))
	}
}

func Div(a, b Number) Number {
	switch a := a.toLowerPrec(b).(type) {
	case *ExactNumber:
		return div(a, b.toLowerPrec(a).(*ExactNumber))
	case *InexactNumber:
		return div(a, b.toLowerPrec(a).(*InexactNumber))
	default:
		panic(fmt.Errorf("should not happen: %T", a))
	}
}
func div[T any, PT _Real[T]](a, b *_Number[T, PT]) *_Number[T, PT] {
	return &_Number[T, PT]{
		RealPart: PT(new(T)).Quo(a.RealPart, b.RealPart),
		ImagPart: PT(new(T)).Quo(a.ImagPart, b.ImagPart),
	}
}

// Page 22, column 2

func (x *_Number[T, PT]) Abs() Number {
	return &_Number[T, PT]{
		RealPart: PT(new(T)).Abs(x.RealPart),
		ImagPart: PT(new(T)).Abs(x.ImagPart),
	}
}

// num = (denom * quo) + Quo
//
// quo(Number) Number // rounded toward zero
// rem(Number) Number // same sign as numerator, 0 <= |rem| < |denom|
// mod(Number) Number // same sign as denominator, 0 <= |mod| < |denom|

// TODO

// func Quo(num, denom Number) Number {}
// func Rem(num, denom Number) Number {}
// func Mod(num, denom Number) Number {}

// Page 23, column 1

// func (x *_Number[T, PT]) Numerator() Number {}
// func (x *_Number[T, PT]) Denominator() Number {}

// func (x *_Number[T, PT]) Floor() Number {}
// func (x *_Number[T, PT]) Ceiling() Number {}
// func (x *_Number[T, PT]) Truncate() Number {}
// func (x *_Number[T, PT]) Round() Number {}

// Page 23, column 2

// func Exp(z Number) Number      {}
// func Log(z Number) Number      {}
// func Sin(z Number) Number      {}
// func Cos(z Number) Number      {}
// func Tan(z Number) Number      {}
// func Asin(z Number) Number     {}
// func Acos(z Number) Number     {}
// func Atan(z Number) Number     {}
// func Atan2(x, y Number) Number {}

// Page 24, column 1

// func Sqrt(z Number) Number     {}
// func Expt(z Number) Number     {}

// (make-rectangular x1 x2) =⇒ z
// (make-polar x3 x4) =⇒ z
// (real-part z) =⇒ x1
// (imag-part z) =⇒ x2
// (magnitude z) =⇒ |x3|
// (angle z) =⇒ xangle


// (exact->inexact z)
// (inexact->exact z)

// (exact->inexact z)
// (inexact->exact z)

// Page 24, column 2

// (number->string z ) procedure
// (number->string z radix ) procedure

// (string->number string) procedure
// (string->number string radix ) procedure
