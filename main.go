package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	if err := MainWithError(bufio.NewReader(os.Stdin)); err != nil {
		fmt.Fprintf(os.Stderr, "%s: error: %v\n", os.Args[0], err)
		os.Exit(1)
	}
}

func MainWithError(r *bufio.Reader) error {
	for {
		o, err := read(r)
		if err != nil {
			return err
		}
		_ = eval(o)
	}
}

func read(r *bufio.Reader) (Object, error) {
	// TODO
}

func eval(Object) Object {
	// TODO
}

// §3.1

type Value interface {
	isValue()
}

type Environment map[string]Value

// §3.2

type Object interface {
	Value
	isObject()
}
