;; §4.2.1. Conditionals
(cond <clause1> <clause2> ...)
(case <key> <clause1> <clause2> ...)
(and <test1> ...)
(or <test1> ...)

;; §4.2.2. Binding constructs
(let <bindings> <body>)
(let* <bindings> <body>)
(letrec <bindings> <body>)

;; §4.2.3. Sequencing
(begin <expression1> <expression2> ...)

;; §4.2.4. Iteration
(do ...)

;; §4.2.5. Delayed evaluation
(delay <expression>)

;; §4.2.6. Quasiquotation
(quasiquote <qqtemplate>)

