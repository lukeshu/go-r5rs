// -*- fill-column: 80 -*-

package main

import (
	"bufio"
	"fmt"
	"regexp"
	"strings"
	"unicode"
	"unicode/utf8"
)

type Runer interface {
	ReadRune() (rune, error)
	UnreadRune()
	PeekRunes(int) []rune
	DiscardRunes(int)
}

func readRune(src *bufio.Reader) (r rune, err error) {
	r, size, err := src.ReadRune()
	if err == nil && r == unicode.ReplacementChar && size == 1 {
		if err := src.UnreadByte(); err != nil {
			return 0, err
		}
		b, err := src.ReadByte()
		if err != nil {
			return 0, err
		}
		if err := src.UnreadByte(); err != nil {
			return 0, err
		}
		err = fmt.Errorf("invalid UTF-8: %#x", b)
	}
	return r, err
}

////////////////////////////////////////////////////////////////////////////////

type TokenType int

const (
	TInvalid TokenType = iota
	TIdentifier
	TBoolean
	TNumber
	TCharacter
	TString
	TLParen  // (
	TRParen  // )
	THLParen // #(
	TRTick   // '
	TLTick   // `
	TComma   // ,
	TCommaAt // ,@
	TDot     // .
)

type Token struct {
	Type TokenType

	IdentifierVal string
	BooleanVal    bool
	NumberVal     NumberToken
	CharacterVal  rune
	StringVal     []rune
}

var reIdentifier = regexp.MustCompile(`(?i)^([a-z!$%&*/:<=>?^_~][a-z!$%&*/:<=>?^_~0123456789+-.@]*|+|-|\.\.\.)$`)

func IsValidIdentifier(str string) bool {
	return reIdentifier.MatchString(str)
}

type Lexer struct {
	r Runer

	lookahead Token
}

func (l *Lexer) ReadToken() (Token, error) {
	if l.lookahead.Type != TInvalid {
		ret := l.lookahead
		l.lookahead = Token{}
		return ret, nil
	}
	return l.readToken()
}

func (l *Lexer) PeekToken() (Token, error) {
	if l.lookahead.Type == TInvalid {
		var err error
		l.lookahead, err = l.readToken()
		if err != nil {
			return Token{}, err
		}
	}
	return l.lookahead, nil
}

func (l *Lexer) readToken() (Token, error) {
again:
	c, err := l.r.ReadRune()
	if err != nil {
		return Token{}, err
	}
	switch c {

	// tokens //////////////////////////////////////////////////////////////
	case // identifier
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		'!', '$', '%', '&', '*', '/', ':', '<', '=', '>', '?', '^', '_', '~':
		var val strings.Builder
		_, _ = val.WriteRune(unicode.ToLower(c))
	idloop:
		for {
			c, err := l.r.ReadRune()
			if err != nil {
				break idloop
			}
			switch c {
			case
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
				'!', '$', '%', '&', '*', '/', ':', '<', '=', '>', '?', '^', '_', '~',
				'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'+', '-', '.', '@':
				_, _ = val.WriteRune(unicode.ToLower(c))
			case ' ', '\n', '(', ')', '"', ';':
				l.r.UnreadRune()
				break idloop
			default:
				return Token{}, fmt.Errorf("invalid character in identifier: %q", c)
			}
		}
		return Token{Type: TIdentifier, IdentifierVal: val.String()}, nil
	case '#':
		c, err := l.r.ReadRune()
		if err != nil {
			return Token{}, err
		}
		switch c {
		case '(':
			return Token{Type: THLParen}, nil
		case 't', 'T':
			return Token{Type: TBoolean, BooleanVal: true}, nil
		case 'f', 'F':
			return Token{Type: TBoolean, BooleanVal: false}, nil
		case '\\': // character
			c, err := l.r.ReadRune()
			if err != nil {
				return Token{}, err
			}
			// These rules are not specified in §7.1. Formal Syntax,
			// but in §6.3.4. Characters.
			switch c {
			case
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
				var _name strings.Builder
				_, _ = _name.WriteRune(c)
			charloop:
				for {
					c, err := l.r.ReadRune()
					if err != nil {
						break charloop
					}
					switch c {
					case
						'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
						'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
						_, _ = _name.WriteRune(c)
					case ' ', '\n', '(', ')', '"', ';':
						l.r.UnreadRune()
						break charloop
					default:
						return Token{}, fmt.Errorf("invalid character in named-character: %q", c)
					}
				}
				name := _name.String()
				if utf8.RuneCountInString(name) == 1 {
					c, _ = utf8.DecodeRuneInString(name)
					return Token{Type: TCharacter, CharacterVal: c}, nil
				}
				switch strings.ToLower(name) {
				case "space":
					return Token{Type: TCharacter, CharacterVal: ' '}, nil
				case "newline":
					return Token{Type: TCharacter, CharacterVal: '\n'}, nil
				default:
					return Token{}, fmt.Errorf("unknown named-character: %q", name)
				}
			default:
				return Token{Type: TCharacter, CharacterVal: c}, nil
			}
		case
			'i', 'e', 'b', 'o', 'd', 'x',
			'I', 'E', 'B', 'O', 'D', 'X':
			l.r.UnreadRune()
			return l.lexNumber()
		}
	case '+', '-', '.':
		peek := l.r.PeekRunes(3)
		switch peek[0] {
		case ' ', '\n', '(', ')', '"', ';':
			switch c {
			case '+':
				return Token{Type: TIdentifier, IdentifierVal: "+"}, nil
			case '-':
				return Token{Type: TIdentifier, IdentifierVal: "-"}, nil
			case '.':
				return Token{Type: TDot}, nil
			}
		case '.':
			if peek[1] == '.' && strings.ContainsRune(` \n()";`, peek[2]) {
				l.r.DiscardRunes(2)
				return Token{Type: TIdentifier, IdentifierVal: "..."}, nil
			} else {
				return Token{}, fmt.Errorf("invalid token: %q", "..")
			}
		default:
			l.r.UnreadRune()
			return l.lexNumber()
		}
	case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
		l.r.UnreadRune()
		return l.lexNumber()
	case '"':
		var val []rune
		for {
			c, err := l.r.ReadRune()
			if err != nil {
				return Token{}, err
			}
			switch c {
			case '"':
				return Token{Type: TString, StringVal: val}, nil
			case '\\':
				c, err := l.r.ReadRune()
				if err != nil {
					return Token{}, err
				}
				switch c {
				case '"', '\\':
					val = append(val, c)
				default:
					return Token{}, fmt.Errorf("invalid backslash-escape in string: %q", c)
				}
			default:
				val = append(val, c)
			}
		}
	case '(':
		return Token{Type: TLParen}, nil
	case ')':
		return Token{Type: TRParen}, nil
	case '\'':
		return Token{Type: TRTick}, nil
	case '`':
		return Token{Type: TLTick}, nil
	case ',':
		c, err := l.r.ReadRune()
		if c == '@' {
			return Token{Type: TCommaAt}, nil
		}
		if err == nil {
			l.r.UnreadRune()
		}
		return Token{Type: TComma}, nil

	// atmosphere //////////////////////////////////////////////////////////
	case ' ', '\n': // whitespace
		goto again
	case ';': // comment
		for {
			c, err := l.r.ReadRune()
			if err != nil {
				return Token{}, err
			}
			if c == '\n' {
				goto again
			}
		}
	default:
		return Token{}, fmt.Errorf("a token cannot begin with character: %q", c)
	}
	panic("not reached")
}

// ⟨num R⟩ → ⟨prefix R⟩ ⟨complex R⟩
type NumberToken struct {
	// ⟨prefix R⟩
	Radix uint8
	Exact bool

	// ⟨complex R⟩
	RealPart string
	Polar    bool
	ImagPart string
}

func (l *Lexer) lexNumber() (Token, error) {
	var ret NumberToken

	// ⟨prefix R⟩ //////////////////////////////////////////////////////////

	ret.Radix = 10 // 2, 8, 10, or 16
	ret.Exact = true
	var haveExact, haveRadix bool
prefix:
	if peek := l.r.PeekRunes(2); peek[0] == '#' {
		if haveExact && haveRadix {
			return Token{}, fmt.Errorf("invalid prefix")
		}
		if !haveExact {
			switch peek[1] {
			case 'i', 'I':
				ret.Exact = false
				haveExact = true
				goto prefix
			case 'e', 'E':
				ret.Exact = true
				haveExact = true
				goto prefix
			}
		}
		if !haveRadix {
			switch peek[1] {
			case 'b', 'B':
				ret.Radix = 2
				haveRadix = true
				goto prefix
			case 'o', 'O':
				ret.Radix = 8
				haveRadix = true
				goto prefix
			case 'd', 'D':
				ret.Radix = 10
				haveRadix = true
				goto prefix
			case 'x', 'X':
				ret.Radix = 16
				haveRadix = true
				goto prefix
			}
		}
		return Token{}, fmt.Errorf("invalid prefix: %q", peek)
	}

	// ⟨complex R⟩ /////////////////////////////////////////////////////////

	// general productions:
	//
	//	⟨complex R⟩ → ⟨real R⟩
	//	            | ⟨real R⟩ @ ⟨real R⟩
	//	            | ⟨real R⟩ + ⟨ureal R⟩ i
	//	            | ⟨real R⟩ - ⟨ureal R⟩ i
	//	            | ⟨real R⟩ + i
	//	            | ⟨real R⟩ - i
	//	            | + ⟨ureal R⟩ i
	//	            | - ⟨ureal R⟩ i
	//	            | + i
	//	            | - i

	if peek := l.r.PeekRunes(2); (peek[0] == '-' || peek[0] == '+') && (peek[1] == 'i' || peek[1] == 'I') {
		ret.ImagPart = string(peek)
		l.r.DiscardRunes(2)
		return Token{Type: TNumber, NumberVal: ret}, nil
	} else {
		var err error
		ret.RealPart, err = l.lexReal(ret.Radix)
		if err != nil {
			return Token{}, err
		}

		peek := l.r.PeekRunes(2)
		switch peek[0] {
		case 'i', 'I':
			l.r.DiscardRunes(1)
			ret.ImagPart = ret.RealPart
			ret.RealPart = ""
		case '@':
			l.r.DiscardRunes(1)
			ret.Polar = true
			imag, err := l.lexReal(ret.Radix)
			if err != nil {
				return Token{}, err
			}
			ret.ImagPart = imag
		case '+', '-':
			switch peek[1] {
			case 'i', 'I':
				ret.ImagPart = string(peek)
				l.r.DiscardRunes(2)
			default:
				imag, err := l.lexReal(ret.Radix)
				if err != nil {
					return Token{}, err
				}
				c, err := l.r.ReadRune()
				if err != nil {
					return Token{}, err
				}
				if c != 'i' && c != 'I' {
					return Token{}, fmt.Errorf("invalid character in number: %q", c)
				}
				ret.ImagPart = imag + "i"
			}
		}
	}

	peek := l.r.PeekRunes(1)
	switch peek[0] {
	case ' ', '\n', '(', ')', '"', ';':
		// ok
	default:
		c := rune(peek[0])
		l.r.DiscardRunes(1)
		return Token{}, fmt.Errorf("invalid character in number: %q", c)
	}

	return Token{Type: TNumber, NumberVal: ret}, nil
}

func hexToLower(r rune) rune {
	if 'A' <= r && r <= 'F' {
		r = 'a' + (r - 'A')
	}
	return r
}

func (l *Lexer) lexReal(radix uint8) (string, error) {
	// general productions:
	//
	//	⟨real R⟩ → ⟨sign⟩ ⟨ureal R⟩
	//
	//	⟨ureal R⟩ → ⟨uinteger R⟩
	//	          | ⟨uinteger R⟩ / ⟨uinteger R⟩
	//	          | ⟨decimal R⟩
	//
	//	⟨uinteger R⟩ → ⟨digit R⟩+ #*
	//
	// base-10 specific productions:
	//
	//	⟨decimal 10⟩ → ⟨uinteger 10⟩ ⟨suffix⟩
	//	             | . ⟨digit 10⟩+ #* ⟨suffix⟩
	//	             | ⟨digit 10⟩+ . ⟨digit 10⟩* #* ⟨suffix⟩
	//	             | ⟨digit 10⟩+ #+ . #* ⟨suffix⟩
	//
	//	⟨suffix⟩ → ⟨empty⟩
	//	         | ⟨exponent marker⟩ ⟨sign⟩ ⟨digit 10⟩+
	//
	//	⟨exponent marker⟩ → e | s | f | d | l

	digits := "0123456789abcdef"[:radix]

	var str strings.Builder
	haveSign := false
	haveDigits := false
	haveDot := false
	inexact := false
	for {
		c, err := l.r.ReadRune()
		if err != nil {
			if str.Len() > 0 {
				return str.String(), nil
			}
			return "", err
		}
		switch {
		case !haveSign && (c == '+' || c == '-'):
			_, _ = str.WriteRune(c)
		case !inexact && strings.ContainsRune(digits, c):
			haveDigits = true
			_, _ = str.WriteRune(c)
		case radix == 10 && !haveDot && c == '.':
			haveDot = true
			_, _ = str.WriteRune(c)
		case haveDigits && c == '#':
			inexact = true
			_, _ = str.WriteRune(c)
		case radix == 10 && strings.ContainsRune("esfdl", c):
			_, _ = str.WriteRune(c)
			haveSign = false
			haveDigits = false
			for {
				c, err := l.r.ReadRune()
				if err != nil {
					return "", err
				}
				switch {
				case !haveSign && (c == '+' || c == '-'):
					_, _ = str.WriteRune(c)
				case strings.ContainsRune(digits, c):
					haveDigits = true
					_, _ = str.WriteRune(c)
				default:
					if !haveDigits {
						return "", fmt.Errorf("invalid decimal suffix")
					}
					return str.String(), nil
				}
			}
		default:
			return str.String(), nil
		}
	}
}
