package main

import (
	"fmt"
)

type Datum interface {
	fmt.Stringer
	isDatum()
}

func Read(r *Lexer) (Datum, error) {
	tok, err := r.ReadToken()
	if err != nil {
		return nil, err
	}
	switch tok.Type {
	case TBoolean:
		return Boolean(tok.BooleanVal), nil
	case TNumber:
		panic("TODO")
	case TCharacter:
		return Character(tok.CharacterVal), nil
	case TString:
		ret := make(String, len(tok.StringVal))
		for i := range tok.StringVal {
			ret[i] = Character(tok.StringVal[i])
		}
		return ret, nil
	case TIdentifier:
		return Symbol(tok.IdentifierVal), nil
	case TLParen:
		var items []Datum
	loop:
		for {
			tok, err := r.PeekToken()
			if err != nil {
				return nil, err
			}
			switch tok.Type {
			case TRParen:
				_, _ = r.ReadToken()
				items = append(items, EmptyList{})
				break loop
			case TDot:
				_, _ = r.ReadToken()
				item, err := Read(r)
				if err != nil {
					return nil, err
				}
				items = append(items, item)
				tok, err := r.ReadToken()
				if err != nil {
					return nil, err
				}
				if tok.Type != TRParen {
					return nil, fmt.Errorf("bad pair")
				}
				break loop
			default:
				item, err := Read(r)
				if err != nil {
					return nil, err
				}
				items = append(items, item)
			}
		}
		ret := Datum(items[len(items)-1])
		for i := len(items) - 1; i >= 0; i-- {
			ret = Pair{Car: items[i], Cdr: ret}
		}
		return ret, nil
	case THLParen:
		var ret Vector
		for {
			tok, err := r.PeekToken()
			if err != nil {
				return nil, err
			}
			switch tok.Type {
			case TRParen:
				_, _ = r.ReadToken()
				return ret, nil
			default:
				item, err := Read(r)
				if err != nil {
					return nil, err
				}
				ret = append(ret, item)
			}
		}
	case TRTick, TLTick, TComma, TCommaAt:
		dat, err := Read(r)
		if err != nil {
			return nil, err
		}
		return Abbreviation(tok.Type, dat), nil
	default:
		return nil, fmt.Errorf("unexpected token: %v", tok)
	}
}

func Abbreviation(typ TokenType, dat Datum) Datum {
	// TODO
	return nil
}
