package main

import (
	"fmt"
	"strings"
)

// 6.3.1. Booleans

type Boolean bool

func (Boolean) isDatum()
func (b Boolean) String() string {
	if b {
		return "#t"
	} else {
		return "#f"
	}
}

// 6.3.2. Pairs and lists

type EmptyList struct{}

func (EmptyList) isDatum()
func (EmptyList) String() string {
	return "()"
}

type Pair struct {
	Car Datum
	Cdr Datum
}

func (Pair) isDatum()

func (p Pair) String() string {
	return "(" + p.stringNoParens() + ")"
}
func (p Pair) stringNoParens() string {
	switch cdr := p.Cdr.(type) {
	case EmptyList:
		return p.Car.String()
	case Pair:
		return p.Car.String() + " " + cdr.stringNoParens()
	default:
		return p.Car.String() + " . " + p.Cdr.String()
	}
}

// 6.3.3. Symbols

type Symbol string

func (Symbol) isDatum()
func (s Symbol) String() string {
	if IsValidIdentifier(string(s)) {
		return "'" + s.String()
	} else {
		return fmt.Sprintf("#<symbol:%q>", s)
	}
}

// 6.3.4. Characters

type Character rune

func (Character) isDatum()
func (c Character) String() string {
	switch c {
	case ' ':
		return `#\space`
	case '\n':
		return `#\newline`
	default:
		return fmt.Sprintf(`#\%c`, c)
	}
}

// 6.3.5. Strings

type String []Character

func (String) isDatum()
func (s String) String() string {
	var ret strings.Builder
	ret.WriteRune('"')
	for _, c := range s {
		switch c {
		case '\\':
			ret.WriteString(`\\`)
		case '"':
			ret.WriteString(`\"`)
		default:
			ret.WriteRune(rune(c))
		}
	}
	ret.WriteRune('"')
	return ret.String()
}

// 6.3.6. Vectors

type Vector []Datum

func (Vector) isDatum()
func (v Vector) String() string {
	var ret strings.Builder
	ret.WriteString("#(")
	for i, item := range v {
		if i > 0 {
			ret.WriteString(" ")
		}
		ret.WriteString(item.String())
	}
	ret.WriteString(")")
	return ret.String()
}
