go-r5rs
-------

A toy implement of R5RS Scheme in Go.

Perhaps practical Scheme implementations should have moved on to
R6RS-small by now, but I chose to implement R5RS because of the
simplicity of the specification.
